#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int createChild(int status)
{
    int rc = fork();
    if (rc == 0) exit(status);
    else printf("Child process created. (pid=%d, status=%d)\n", rc, status);
    return rc;
}

int* createChildren(int count)
{
    // Allocate memory for child-process ids
    int* pids = malloc(sizeof(int) * count);

    for (int i = 0; i < count; i++)
    {
        int rc = createChild(i);

        // Return NULL if this is a child process to stop.
        if (rc == 0) return NULL;
        
        // Write the child process id to memory.
        pids[i] = rc;
    }

    return pids;
}

int main() {
    int childCount = 5;

    int* pids = createChildren(childCount);

    // Stop the process if this is a child process.
    if (pids == NULL) return 0;
    
    // Wait for all child processes to stop.
    for (int i = 0; i < childCount; i++)
    {
        int status;
        waitpid(pids[i], &status, 0);
        printf("Child process terminated. (pid=%d, status=%d)\n", pids[i], WEXITSTATUS(status));
    }

    return 0;
}
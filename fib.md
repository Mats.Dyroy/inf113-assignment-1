# A temporary solution to fib.c

## Locating the problem
`./fib 0` : `0`  
`./fib 1` : `9`  
`./fib 10` : `5`  
`./fib 100` : `5`  
`./fib 1000` : `5`  
`./fib 10000` : `5`  
`./fib 100000` : `5`  
`./fib 1000000` : `Segmentation fault (core dumped)`  

## Problem range
This range is spesific to my computer and setup. The leasson learned is that large inputs produce segmentation faults.  
`./fib 174568` : `1`  
`./fib 174569` : `Segmentation fault (core dumped)`

## Goal
Use the `ulimit` linux tool to delay the segmentation fault problem.
1. `ulimit -s 16000` (*give process 16MB stack size*)
2. `./fib 174569` (*test effect*)
3. `./fib 174569` : `9` (*success*)
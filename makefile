compile:
	gcc -g process.c -o process
	gcc -g swap.c -o swap
	gcc -g uppercase.c -o uppercase
	gcc -g prime.c -o prime
	gcc -g fib.c -o fib

clean:
	rm -f a.out
	rm -f process
	rm -f swap
	rm -f uppercase
	rm -f prime
	rm -f fib
# Question 1
## What does the *user time* column tell you?
Time spent running user (*non-kernel*) code.

## How does the execution of more than one instance of *mem* affect the numbers in the *user time* column?
It goes down due to the **mem** instances having to share time and overhead from switching.

## What happens if you change the number of processors allocated to the VM?
The **user time** goes up as the instances can run in parallell.

# Question 2
**free** before running `./mem 1024` was `129788`.  
After running `./mem 1024`, **free** dropped to `126168`, then `124332`, then `123508` before stabalizing at `122764`.
This gives a total decline of `7024 KB`, a lot less than the maybe expected `1024 MB`.

# Question 3
Before running a memory intencive operation (`./mem  4000`), **si** was `0` and **so** was `6`.  
After running `./mem 4000`, **si** and **so** spiked sharply to `268` and `215800` respectivly.
After `./mem 4000` completed and was killed, **si** spiked again to `13996` while **so** returned to `0`.  
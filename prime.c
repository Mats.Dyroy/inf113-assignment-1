#include <stdio.h>
#include <stdlib.h>

int main() {

    int l, h;
    if (scanf("%d %d", &l, &h) < 2 || h < 2)
        return 1;

    // Allocated enough space (h+1 instead of just h)
    int *A = (int *)malloc(sizeof(int) * (h + 1));
    A[0] = 0, A[1] = 0;
    for (int i = 2; i <= h; i++)
        A[i] = 1;

    for (int i = 2; i * i <= h; i++) {
        if (A[i]) {
            for (int j = i * i; j <= h; j += i) {
                A[j] = 0;
            }
        }
    }

    for (int i = l; i <= h; i++) {
        if (A[i])
            printf("%d ", i);
    }
    printf("\n");

    // Fixed heap leak
    free(A);

    return 0;
}

//
// Before fixes heap leak
//
// ==17628== Memcheck, a memory error detector
// ==17628== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
// ==17628== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
// ==17628== Command: ./prime
// ==17628== 
// 1 13
// ==17628== Invalid write of size 4
// ==17628==    at 0x109264: main (prime.c:13)
// ==17628==  Address 0x4a954b4 is 0 bytes after a block of size 52 alloc'd
// ==17628==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==17628==    by 0x10922A: main (prime.c:10)
// ==17628== 
// ==17628== Invalid read of size 4
// ==17628==    at 0x1092FB: main (prime.c:24)
// ==17628==  Address 0x4a954b4 is 0 bytes after a block of size 52 alloc'd
// ==17628==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==17628==    by 0x10922A: main (prime.c:10)
// ==17628== 
// 2 3 5 7 11 13 
// ==17628== 
// ==17628== HEAP SUMMARY:
// ==17628==     in use at exit: 52 bytes in 1 blocks
// ==17628==   total heap usage: 3 allocs, 2 frees, 2,100 bytes allocated
// ==17628== 
// ==17628== 52 bytes in 1 blocks are definitely lost in loss record 1 of 1
// ==17628==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==17628==    by 0x10922A: main (prime.c:10)
// ==17628== 
// ==17628== LEAK SUMMARY:
// ==17628==    definitely lost: 52 bytes in 1 blocks
// ==17628==    indirectly lost: 0 bytes in 0 blocks
// ==17628==      possibly lost: 0 bytes in 0 blocks
// ==17628==    still reachable: 0 bytes in 0 blocks
// ==17628==         suppressed: 0 bytes in 0 blocks
// ==17628== 
// ==17628== ERROR SUMMARY: 3 errors from 3 contexts (suppressed: 0 from 0)
// ==17628== 
// ==17628== 1 errors in context 1 of 3:
// ==17628== Invalid read of size 4
// ==17628==    at 0x1092FB: main (prime.c:24)
// ==17628==  Address 0x4a954b4 is 0 bytes after a block of size 52 alloc'd
// ==17628==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==17628==    by 0x10922A: main (prime.c:10)
// ==17628== 
// ==17628== 
// ==17628== 1 errors in context 2 of 3:
// ==17628== Invalid write of size 4
// ==17628==    at 0x109264: main (prime.c:13)
// ==17628==  Address 0x4a954b4 is 0 bytes after a block of size 52 alloc'd
// ==17628==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==17628==    by 0x10922A: main (prime.c:10)
// ==17628== 
// ==17628== ERROR SUMMARY: 3 errors from 3 contexts (suppressed: 0 from 0)

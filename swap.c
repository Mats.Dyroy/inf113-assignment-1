#include <stdio.h>

void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int main() {
    int *a = NULL, *b = NULL, x, y;

    if (scanf("%d %d", &x, &y) < 2)
        return 1;

    // Was: a = &x, a = &y;
    // Problem: Program always resulted in segmentation fault.
    // Solution: Writa x to a and y to b.
    a = &x, b = &y;
    swap(a, b);

    printf("%d %d\n", *a, *b);

    return 0;
}

// ==12501== Memcheck, a memory error detector
// ==12501== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
// ==12501== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
// ==12501== Command: ./a.out
// ==12501== 
// 1 2
// 2 1
// ==12501== 
// ==12501== HEAP SUMMARY:
// ==12501==     in use at exit: 0 bytes in 0 blocks
// ==12501==   total heap usage: 2 allocs, 2 frees, 2,048 bytes allocated
// ==12501== 
// ==12501== All heap blocks were freed -- no leaks are possible
// ==12501== 
// ==12501== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
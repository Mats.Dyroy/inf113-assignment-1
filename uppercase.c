#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>

int main() {
    char *s = NULL;
    size_t n = 0, l;

    l = getline(&s, &n, stdin);
    for (int i = 0; i < l; i++)
        s[i] = toupper(s[i]);
    printf("%s", s);

    // Problem: Char buffer containing the input string was never released.
    // Solution: Free the memory manually.
    free(s);

    return 0;
}

//
// BEFORE MODIFICATION:
//
// ==12636== Memcheck, a memory error detector
// ==12636== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
// ==12636== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
// ==12636== Command: ./a.out
// ==12636== 
// hello world
// HELLO WORLD
// ==12636== 
// ==12636== HEAP SUMMARY:
// ==12636==     in use at exit: 120 bytes in 1 blocks
// ==12636==   total heap usage: 3 allocs, 2 frees, 2,168 bytes allocated
// ==12636== 
// ==12636== 120 bytes in 1 blocks are definitely lost in loss record 1 of 1
// ==12636==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
// ==12636==    by 0x48EA1A2: getdelim (iogetdelim.c:62)
// ==12636==    by 0x1091ED: main (in /home/mats/Documents/inf113-assignment-1/a.out)
// ==12636== 
// ==12636== LEAK SUMMARY:
// ==12636==    definitely lost: 120 bytes in 1 blocks
// ==12636==    indirectly lost: 0 bytes in 0 blocks
// ==12636==      possibly lost: 0 bytes in 0 blocks
// ==12636==    still reachable: 0 bytes in 0 blocks
// ==12636==         suppressed: 0 bytes in 0 blocks
// ==12636== 
// ==12636== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
